
# Requirements
- Latest version of NodeJS v16+

# Usage:
```javascript
    nodejs testrng range selections replacements draws
```
where:
- range, output range from 1 to range (inclusive)
- selections, number of selections
- replacements, yes (replacements are allowed), no otherwise
- draws, number of draws.

### Output:
    This tool will produce draws number of selections in each line separated by space character. You can redirect output to a file

## Example:
```
    nodejs testrng 50 4 no 100
``` 

This will produce 100 lines, each line containing 4 selections out of 50 without replacements.


