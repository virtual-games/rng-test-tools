'use strict';
const { randomInt } = require("crypto");

module.exports.generate = async function(range, selections, replacements){
    var result = [];
    var low = 1;
    var high = range;
    if (replacements){
      for (let i = 0; i <  selections; i++) {
        var rnd = await randomInt(low, high);
        result.push(rnd);
      }
    }else{

      let i = 0;
      while (i < selections){
        var rnd = await randomInt(low, high);
        if(result.indexOf(rnd) === -1) {
          result.push(rnd);
          i++;
        }
      }
    }
    return result;
}