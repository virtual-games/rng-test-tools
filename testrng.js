
var rng = require("./rng");

test = async (range, selections, replacements, draws) => {
  for(var i = 0; i<draws; i++){
    var result = await rng.generate(range, selections, replacements);
    console.log(result.join(' '));
  }
}

var myArgs = process.argv.slice(2);
range = Number(myArgs[0]);
selections = Number(myArgs[1]);
replacements = myArgs[2];
draws = Number(myArgs[3]);

if (range === undefined || selections === undefined || replacements === undefined || draws === undefined || range <= 0 || selections <= 0 || (replacements != "yes" && replacements != "no") || draws <= 0){
  console.log("Usage:");
  console.log("nodejs testrng range selections replacements draws");
  console.log(" - range > 0");
  console.log(" - selections > 0");
  console.log(" - replacements yes|no");
  console.log(" - draws > 0");
  return;
}

test(range, selections, replacements == "yes", draws);
